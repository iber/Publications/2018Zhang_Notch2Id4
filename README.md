**Source codes related to the manuscript:**
==============================================

Zhang et al. *Notch2 signaling regulates Id4 and cell cycle genes to maintain neural stem cell quiescence in the adult hippocampus.*


**Data processing:**
====================
Raw data is available at GEO:....

#### Raw data (.fastq files) were mapped using Bowtie2 algorithm. 
See shell script: bowtie2_processing.sh

#### Gene counts were generated from mapped data using HTSeq algorithm.
See shell script: htseq_processing.sh

#### For each .fastq file a .count were generated. 
See /counts folder

**Analysis**
============
See SuvrelNotch2KO.ipynb for Suvrel analysis and figures. 




